# URI Lab Bootstrap

Installs the Private Key from an azure vault and the lab-update package.

## Usage


**bootstrap**

    bin/bootstrap.sh 6
    where '6' is the lab number.



### Sample Output



## Install
    
    Go to https://bitbucket.com/uri-labs
    
    Click 'lab-bootstrap' 
    Click 'Downloads'
    Click 'Download repository'
    
## Pre-requisites

    1. Create an Azure Key Vault
    2. Upload your private key to the key vaul as a secret


    az login
    az account set --subscription "YOUR SUBSCRIPTION NAME"
    az keyvault secret set --vault-name 'YOUR_VAULT_NAME' -n 'YOUR_SECRET_NAME' -f 'YOUR_LOCAL_PATH_TO_PRIVATE_KEY'

### Configure



create a copy of the configuration example

    cp .bin/bootstrap/config-example.conf .bin/bootstrap/config.conf

edit with your custom settings 

    nano .bin/bootstrap/config.conf


## ToDo / Known Issues








