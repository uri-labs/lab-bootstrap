#!/bin/bash


# open browser Mac: open "http://www.google.com" 
# docker: https://store.docker.com/editions/community/docker-ce-desktop-mac 


#############################
#
# Functions used in install
#
#############################





####################
function ask (){
####################


read -p "${1}" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
return 0;
else 
return 1;
fi


}


####################
function installme (){
####################



if ask "install?" ; then

echo -e "\n do something"
else
echo -e "\nskipping"
fi



}



##########################
function checkWindows (){
##########################


case "$(uname -s)" in

   Darwin)
     return 1;
     ;;

   Linux)
     return 1;
     ;;

   CYGWIN*|MINGW32*|MSYS*|MINGW64*)
     return 0
     ;;

   # Add here more strings to compare
   # See correspondence table at the bottom of this answer

   *)
     return 1;
     ;;
esac



}

#####################
function checkMac (){
#####################


case "$(uname -s)" in

   Darwin)
     return 0;
     ;;

   Linux)
     return 1;
     ;;

   CYGWIN*|MINGW32*|MSYS*|MINGW64*)
     return 1
     ;;

   # Add here more strings to compare
   # See correspondence table at the bottom of this answer

   *)
     return 1;
     ;;
esac



}

#####################
function checkLinux (){
#####################


case "$(uname -s)" in

   Darwin)
     return 1;
     ;;

   Linux)
     return 0;
     ;;

   CYGWIN*|MINGW32*|MSYS*|MINGW64*)
     return 1
     ;;

   # Add here more strings to compare
   # See correspondence table at the bottom of this answer

   *)
     return 1;
     ;;
esac



}

function dockerLogin () {

echo "Logging into Docker Hub with username ${DOCKER_USERNAME}"
docker login -u ${DOCKER_USERNAME}

if [ $? -eq 0 ]; then
    echo OK
else
    echo "FAIL - Docker Login Failed. Trying winpty ..."
    echo "for more information on this error see https://willi.am/blog/2016/08/08/docker-for-windows-interactive-sessions-in-mintty-git-bash/"
    
    echo "Enter Docker Hub password for username ${DOCKER_USERNAME}"
    winpty docker login -u ${DOCKER_USERNAME}

fi

if [ $? -eq 0 ]; then
    echo ""
else
    echo FAIL - Docker Login Failed for user ${DOCKER_USERNAME}.

fi

}


# will check for installation file,if not  present, prompts for install.
function checkInstall(){

echo 'checking for previous installation...'
# check whether to run install script.
if [ -e "${DIR}"/"${CACHE_DIR}"/"installed" ]
then
    #skip
    echo 'already installed, continuing...';
    return 0;
else
    echo 'No previous installation detected..'
    touch "${DIR}"/"${CACHE_DIR}"/"installed"
    install;
fi

}



function resetDocs(){
# Resets the participants docs folder


# make sure the document folder is set, otherwise, set it. this is so we don't delete the entire project
if [ -z "${DOCS_DIRNAME}" ]; then DOCS_DIRNAME='docs';  fi


echo "Replacing lab docs folder with fresh copy"
# Copy over the pristine folder 
rm -rf "${LAB_DIR}"/"${DOCS_DIRNAME}"/ > /dev/null && cp -r "${DIR}"/lab-docs "${LAB_DIR}"/"${DOCS_DIRNAME}"
}



function diffList(){
# will return the difference between 2 text strings
: ${1?"Usage: $0 diffList list1 list2"}
: ${2?"Usage: $0 diffList list1 list2"}

local list1="${1}"
local list2="${2}" # should be complete 'path' to object

local diff_list=$(grep -Fxv -f  <(echo -e "${list1}") <(echo -e "${list2}"))

echo "${diff_list}"

}