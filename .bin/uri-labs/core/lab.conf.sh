#!/bin/bash


# open browser Mac: open "http://www.google.com" 
# docker: https://store.docker.com/editions/community/docker-ce-desktop-mac 


#############################
#
# Builds configuration variables that depend on lab.conf
#
#############################




mkdir -p "${TEMPLATES_DIR}"
mkdir -p "${CACHE_DIR}"
mkdir -p "${SECRETS_DIR}"
SA_KEY_FILE="${SECRETS_DIR}"/"${SA_KEY_FILE}"
# for backward compat
IAM_ACCOUNT="${SERVICE_ACCOUNT}"


#############################
#
# DEV Environment Override
#
#############################

## If you want to make this a dev environment, 
## add a file 'dev-env' (no extention) to the .support directory.
## add it to .gitignore so when you do a commit, it doesn't carry through to the live env.
if [ -e "${DIR}"/"dev-env" ]
then
    DEV_ENV=true;
else
    DEV_ENV=false;
fi

if "${DEV_ENV}"; then
   echo "Setting DEV_ENV=true and running setup script in development environment...Change by deleting .support/dev-env file"
   source "${DIR}"/"dev-env"

else
   echo "running script in live environment...Change by adding .support/dev-env file"
fi






