#!/bin/bash




#############################
#
# Required libraries
#
#############################


THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
THIS_PARENT_DIR=$( dirname "${THIS_DIR}" )


# add lab.conf
source "${THIS_DIR}/config-adv.conf"
source "${THIS_DIR}/uri-labs/core/lab.conf.sh"

# include required functions
source "${THIS_DIR}/uri-labs/core/functions.core.sh"
